# Wind server

This is a toy project, made to get my hands on sockets in Python. I downloaded a dataset regarding wind turbines from https://opendata-renewables.engie.com/, containing the measure of different characteristics in time. A span of one month, for just one turbine, has been extracted (details in `analysis.ipynb`) and the power generated against the wind speed has been plotted. 

![img_wind_power](./img_expected_plot.png)

The server process takes this small sample dataset and serves (every ~3 seconds, simulating a data stream from a sensor) a row to the client; this code is in `run_server.py`.

The client, on the other side, connects its socket to the server and reads the row. 
The data got in this way are then plotted "in real time", leading with the time passing to the plot shown above. 
The library used is Bokeh, and a _bokeh server_ is created whose data is updated by a threaded function (containing the client socket call). This code is in `run_client.py`.

In order to make the project portable, [pipenv](https://packaging.python.org/tutorials/managing-dependencies/) is used. Install it with 
```shell
pip install --user pipenv
```

and run in two different terminal emulators:
```
pipenv run python run_server.py localhost 65432
```

and 
```
pipenv run bokeh serve run_client.py --show --args localhost 65432
```

(it is important that the port - here arbitrarily chosen as 65432 - is the same in the two calls, and that the server is run some seconds before the client, in order for the former to be up and running when the client tries to connect).

(If you don't want to use `pipenv`, installing `pandas` and `bokeh` should be enough, and `pipenv run` can be then removed from the commands above.)
