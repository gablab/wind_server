#!/usr/bin/python3
"""
run_server.py

Serve data as rows from a csv, with a socket.
"""

import sys
import pandas as pd
from messaging import Server
NAME_FILE = 'wind_turbine_1.csv'

DF = pd.read_csv(NAME_FILE)
ROWS = [DF.iloc[i] for i in range(len(DF))]
STRING_ROWS = [','.join([str(ir) for ir in r])+'\n' for r in ROWS]
STREAM = STRING_ROWS

with open('df_columns.dat', 'w') as f:
    f.write(','.join(list(DF.columns)))

HOST = sys.argv[1]
PORT = sys.argv[2]
SERV = Server(HOST, int(PORT))
SERV.set_stream(STREAM)
SERV.run()
