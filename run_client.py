"""
run_client.py

Get data from a server, process it and plot it in a Bokeh application.
"""


import sys
from functools import partial
from threading import Thread

from bokeh.models import ColumnDataSource
from bokeh.plotting import curdoc, figure

from tornado import gen

import pandas as pd
from messaging import Client


DATA_SOURCE = ColumnDataSource(data=dict(x_plot=[0], y_plot=[0]))
BOKEH_DOC = curdoc()

@gen.coroutine
def update(x_update, y_update):
    """
    Update the ColumnDataSource used for the plot. The plot updates automatically.
    """
    DATA_SOURCE.stream(dict(x_plot=[x_update], y_plot=[y_update]))


def client_requests(cli_host, cli_port):
    """
    Create a client, request from server and update data for plot.
    """
    while True:
        # get data from server
        cli = Client(cli_host, int(cli_port))
        cli.run()
        response = cli.buffer
        new_list = response[:-2].split(',')


        # create dataframe
        df_input = pd.read_csv('df_columns.dat')
        new_row = pd.DataFrame([new_list], columns=df_input.columns)
        df_input = df_input.append(new_row, ignore_index=True)


        # update the Bokeh document from callback
        BOKEH_DOC.add_next_tick_callback(partial(
            update,
            x_update=pd.to_numeric(df_input.Ws_avg[0]),
            y_update=pd.to_numeric(df_input.P_avg[0])
            ))

P = figure(x_axis_label='Wind speed [m/s]', y_axis_label='Power [kW]')
P.circle(x='x_plot', y='y_plot', fill_alpha=0.5, fill_color=None, source=DATA_SOURCE)

BOKEH_DOC.add_root(P)

HOST = sys.argv[1]
PORT = sys.argv[2]
THREAD = Thread(target=client_requests, args=(HOST, PORT))
THREAD.start()
