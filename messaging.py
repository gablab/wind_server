"""
messaging.py

Provides a Client and a Server objects.
"""

import socket
import types
import selectors
import time

class Server:
    """
    A Server class.
    Create an object with new `serv = Server(host, port)`
    and run it with `serv.run()`; it can be stopped with a keyboard interrupt.
    **Warning**: it does not handle endianness; it dows not take care of security.
    """

    def __init__(self, host, port, stream=None):
        self.sel = selectors.DefaultSelector()
        self.counter = 0
        self.set_stream(stream)
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.bind((host, port))
        self.sock.listen()
        self.sock.setblocking(False)
        self.sel.register(self.sock, selectors.EVENT_READ, data='accept')

    def set_stream(self, stream):
        """
        Set the content to be sent to requesting clients. `stream` should be an iterable.
        """
        self.stream = stream

    def run(self):
        """
        Run the server. Stop with keyboard interruption, or when a connection breaks.
        """
        keyword_to_function = {
            'accept': self.accept,
            'serve': self.serve,
            'write': self.write,
            'read': self.read
        }
        try:
            while True:
                events = self.sel.select(timeout=None)
                #print(len(events))
                for ev_i in events:
                    key, _ = ev_i
                    callback_name = key.data
                    print(callback_name)
                    keyword_to_function[callback_name](ev_i)
                time.sleep(3)
        except KeyboardInterrupt:
            print('keyboard interrupt')
        except IOError:
            print('closed connection')
        finally:
            print('closing server')
            self.close()

    def accept(self, selected):
        """
        Accept a connection.
        """
        key, _ = selected
        sock, _ = key.fileobj.accept()
        self.sel.register(sock, selectors.EVENT_READ | selectors.EVENT_WRITE, data='serve')

    def read(self, selected):
        """
        Read from client.
        """
        key, mask = selected
        if mask & selectors.EVENT_READ:
            recv_data = key.fileobj.recv(2048)
            if len(recv_data) == 0:
                self.sel.unregister(key.fileobj)
                key.fileobj.shutdown(socket.SHUT_RDWR)
                key.fileobj.close()
            #print(recv_data)

    def write(self, selected):
        """
        Write to client.
        """
        key, mask = selected
        if mask & selectors.EVENT_WRITE:
            num_sent = 0
            out_buf = self.stream[self.counter]
            #print('write ', out_buf, len(out_buf))
            while out_buf is not None and num_sent < len(out_buf) - 1:
                out_data = out_buf[num_sent:]
                out_data = out_data.encode('utf-8')
                sent = key.fileobj.send(out_data)
                num_sent += sent
                #print(sent)
            self.counter += 1
            self.close_connection(key.fileobj)

    def serve(self, selected):
        """
        Read and write, connecting with client.
        """
        key, mask = selected
        self.read((key, mask))
        self.write((key, mask))

    def close_connection(self, sock):
        """
        Close connection with client socket.
        """
        self.sel.unregister(sock)
        sock.shutdown(socket.SHUT_RDWR)
        sock.close()

    def close(self):
        """
        Close server socket.
        """
        self.sel.unregister(self.sock)
        self.sock.shutdown(socket.SHUT_RDWR)
        self.sock.close()
        self.sel.close()


class Client:
    """
    A Client getter class.
    Create an object with new `serv = Client(host, port)`
    and run it with `serv.get()`.
    **Warning**: it does not handle endianness; it dows not take care of security.
    """

    def __init__(self, host, port, stream=None):
        self.sel = selectors.DefaultSelector()
        self.counter = 0
        self.set_stream(stream)
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.setblocking(False)
        self.sock.connect_ex((host, port))
        data = types.SimpleNamespace(request='GET', callback='query')
        self.sel.register(self.sock, selectors.EVENT_READ | selectors.EVENT_WRITE, data=data)
        self.stream = data.request
        self.buffer = ''

    def set_stream(self, stream):
        """
        Set the content to be sent to requesting clients. `stream` should be an iterable.
        """
        self.stream = stream

    def run(self):
        """
        Run the server. Stop when a newline '\n' character is read (end of message).
        """
        keyword_to_function = {
            'query': self.query,
            'write': self.write,
            'read': self.read
        }
        try:
            while True:
                events = self.sel.select(timeout=None)
                for ev_i in events:
                    key, mask = ev_i
                    callback_name = key.data.callback
                    print(callback_name)
                    keyword_to_function[callback_name](key, mask)
                    time.sleep(1)
        except KeyboardInterrupt:
            print('keyboard interrupt')
            print('closing server')
            #self.close('closing server')
        except IOError:
            print('closed connection')
            print('closing server')
            self.close()
        finally:
            self.sel.close()


    def read(self, key, mask):
        """
        Read from server.
        """
        if mask & selectors.EVENT_READ:
            recv_data = key.fileobj.recv(2048)
            if recv_data:
                self.buffer += recv_data.decode('utf-8')
            if not recv_data or self.buffer[-1] == '\n':
                self.close()

    def write(self, key, mask):
        """
        Write to server.
        """
        if mask & selectors.EVENT_WRITE:
            if self.stream is not None and self.counter < len(self.stream) - 1:
                out_data = self.stream[self.counter:]+'\n'
                out_data = str(out_data).encode('utf-8')
                sent = key.fileobj.send(out_data)
                self.counter += sent

    def query(self, key, mask):
        """
        Read and write, connection to server.
        """
        self.read(key, mask)
        self.write(key, mask)

    def close(self):
        """
        Close client socket.
        """
        self.sel.unregister(self.sock)
        self.sock.shutdown(socket.SHUT_RDWR)
        self.sock.close()
        self.sel.close()
        raise KeyboardInterrupt
